#include "patchwork.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define UNUSED(something) (void)(something)

static inline struct primitif get_dummy_pritimif(void)
{
    struct primitif p;
    p.nature = NB_NAT_PRIMITIFS;
    p.orientation = NB_ORIENTATIONS;
    return p;  //Renvoie un primitif "bidon"
}

static inline primitif get_primitif(const struct patchwork* const p, size_t x, size_t y)
{
    if (x < p->largeur && y < p->hauteur)
    {
	return List_primitif_get(p->primitifs, p->largeur * y + x);
    }
    else
    {
	return get_dummy_pritimif();
    }
}

// precond: nat ok, verifiee a la construction
struct patchwork *creer_primitif(const enum nature_primitif nat)
{	
    struct patchwork* pt = malloc(sizeof(struct patchwork));
    pt->hauteur = pt->largeur = 1;
    pt->primitifs = List_primitif_new();
    struct primitif tres_primitif;
    tres_primitif.nature = nat;
    tres_primitif.orientation = EST;
    List_primitif_set(pt->primitifs, 0, tres_primitif);
    return pt;
}

static inline enum orientation_primitif rotation_orientation_primitif(enum orientation_primitif prim)
{
    prim++;
    if (prim == NB_ORIENTATIONS)
    {
	prim = EST;
    }
    return prim;
}

// precond: p valide
struct patchwork *creer_rotation(const struct patchwork *old)
{	
    if (old == NULL)
    {
	return NULL;
    }
    struct patchwork* pt = malloc(sizeof(struct patchwork));
    pt->hauteur = old->largeur;
    pt->largeur = old->hauteur;
    pt->primitifs = List_primitif_new();
    for (size_t y = 0; y < pt->hauteur; ++y)
    {
	for (size_t x = 0; x < pt->largeur; ++x)
	{
	    primitif prim = get_primitif(old, old->largeur - y - 1, x);
	    prim.orientation = rotation_orientation_primitif(prim.orientation);
	    List_primitif_push_back(pt->primitifs, prim);
	}
    }
    return pt;
}

void fail_with_error(const char* s)
{
    printf("%s\n", s);
    exit(1);   //OSEF de la mémoire perdue, elle va être récupérée par l'OS juste après de toute façon.
}

// precond: p_g et p_d valides
struct patchwork *creer_juxtaposition(const struct patchwork *p_g,
				      const struct patchwork *p_d)
{
    if (p_g == NULL || p_d == NULL)
    {
	return NULL;
    }
    if (p_g->hauteur != p_d->hauteur)
	fail_with_error("Erreur: deux patchwork de hauteur différente juxtaposés");
    struct patchwork* pt = malloc(sizeof(struct patchwork));
    pt->largeur = p_g->largeur + p_d->largeur;
    pt->hauteur = p_g->hauteur;
    pt->primitifs = List_primitif_new();
    primitif bidon = get_dummy_pritimif();
    List_primitif_resize(pt->primitifs, pt->largeur * pt->hauteur, bidon);
    struct primitif *ptr_new = pt->primitifs->ptrMemory;
    const struct primitif *ptr_gauche = p_g->primitifs->ptrMemory,
	*ptr_droit = p_d->primitifs->ptrMemory;
    const size_t nb_bytes_gauche = p_g->largeur * sizeof(struct primitif);
    const size_t nb_bytes_droit = p_d->largeur * sizeof(struct primitif);
    //Composition des nouvelles rangées de patchwork: pour chaque rangée i
    //on juxtapose la rangée i du patchwork de gauche et la rangée i du patchwork de droite.
    for (size_t y = 0; y < pt->hauteur; ++y)
    {
	memcpy(ptr_new, ptr_gauche, nb_bytes_gauche);
	ptr_new += p_g->largeur;
	ptr_gauche += p_g->largeur;
	memcpy(ptr_new, ptr_droit, nb_bytes_droit);
	ptr_new += p_d->largeur;
	ptr_droit += p_d->largeur;
    }
    return pt;
}


// precond: p_h et p_b valides
struct patchwork *creer_superposition(const struct patchwork *p_h,
                                      const struct patchwork *p_b)
{
    if (p_h == NULL || p_b == NULL)
    {
	return NULL;
    }
    if (p_h->largeur != p_b->largeur)
	fail_with_error("Erreur: deux patchwork de largeur différente superposés");
    struct patchwork* pt = malloc(sizeof(struct patchwork));
    pt->largeur = p_b->largeur;
    pt->hauteur = p_h->hauteur + p_b->hauteur;
    pt->primitifs = List_primitif_new();
    const struct primitif dummy = get_dummy_pritimif();
    List_primitif_resize(pt->primitifs, pt->largeur * pt->hauteur, dummy);
    const size_t nb_bytes_haut = sizeof(struct primitif) * p_h->largeur * p_h->hauteur;
    const size_t nb_bytes_bas = sizeof(struct primitif) * p_b->largeur * p_b->hauteur;
    //Recopie des éléments des deux patchworks, celui du haut puis celui du bas.
    memcpy(pt->primitifs->ptrMemory, p_h->primitifs->ptrMemory, nb_bytes_haut);
    memcpy(pt->primitifs->ptrMemory + p_h->largeur * p_h->hauteur, p_b->primitifs->ptrMemory, nb_bytes_bas);

    return pt;
}


void liberer_patchwork(struct patchwork *patch)
{
    List_primitif_delete(patch->primitifs);
    free(patch);
}
