#include "ast.h"

#define UNUSED(qq) (void)(qq)

/* constantes pour l'affichage des noms */
static const char *noms_primitifs[NB_NAT_PRIMITIFS] = {
    "carre",
    "triangle"
};

static const char *noms_operations[NB_OPERATIONS] = {
    "ROT",
    "JUXT",
    "SUPER"
};



/*---------------------------------------------------------------------------*/
/*     FONCTIONS PORTEES PAR LES NOEUDS                                      */
/*  fonctions suivant les signatures des champs afficher et evaluer          */
/*  definies dans ast.h, adaptees aux differentes natures de noeud d'un ast  */
/*---------------------------------------------------------------------------*/

/*----------- Affichage */
/* Fonctions "specifiques" locales (static) d'affichage d'un noeud
   selon sa nature. Declarees ici, definies plus bas */
static void afficher_valeur(struct noeud_ast *ast);
static void afficher_unaire(struct noeud_ast *ast);
static void afficher_binaire(struct noeud_ast *ast);


/*----------- Evaluation */
/* Fonctions "specifiques" locales (static) d'evaluation d'un noeud
   selon son type. Declarees ici, definies plus bas */
static struct patchwork *evaluer_valeur(struct noeud_ast *ast);
static struct patchwork *evaluer_unaire(struct noeud_ast *ast);
static struct patchwork *evaluer_binaire(struct noeud_ast *ast);



/*----------- Creation des patchworks */

/* Types des pointeurs sur les fonctions de creation des patchworks.
 * Les signatures different selon les noeuds.
 * Les fonctions specifiques sont definies ds le module patchwork.o */
typedef struct patchwork *(*creer_patchwork_valeur_fct) 
    (const enum nature_primitif);
typedef struct patchwork *(*creer_patchwork_unaire_fct)
    (const struct patchwork *);
typedef struct patchwork *(*creer_patchwork_binaire_fct)
    (const struct patchwork *,
     const struct patchwork *);



/*---------------------------------------------------------------------------*/
/*    STRUCTURES DES NOEUDS                                                  */
/*---------------------------------------------------------------------------*/

enum nature_noeud {
    VALEUR,         /* feuille */
    OPERATION,      /* noeud interne */
    NB_NAT_NOEUDS   /* sentinelle */
};

enum arite_operation {
    UNAIRE,
    BINAIRE,
    NB_ARITES_OPERATIONS   /* sentinelle */
};


struct operation_unaire {
    struct noeud_ast *operande;
    creer_patchwork_unaire_fct creer_patchwork;
};

struct operation_binaire {
    struct noeud_ast *operande_gauche;
    struct noeud_ast *operande_droit;
    creer_patchwork_binaire_fct creer_patchwork;
};

struct operation {
    enum arite_operation arite;
    union {
	struct operation_unaire  oper_un;
	struct operation_binaire oper_bin;
    } u;
};


struct valeur {
    enum nature_primitif nature;
    creer_patchwork_valeur_fct creer_patchwork;
};


struct noeud_ast_data {
    const char *nom;

    // nature du noeud: VALEUR ou OPERATION
    enum nature_noeud nature;
    // selon la nature, les donnees representant le noeud
    union {
	struct valeur val;       // si nature == VALEUR
	struct operation oper;   // si nature == OPERATION
    } u;
};



/*---------------------------------------------------------------------------*/
/*     AFFICHAGE                                                             */
/*---------------------------------------------------------------------------*/


/* Definitions (locale) des fonctions "specifiques" 
 * Pas de verif sur le type d'ast, ces fonctions ont ete "branchees" sur
 * les noeud de type adequat lors de leur construction */
static void afficher_valeur(struct noeud_ast *ast)
{
    UNUSED(ast);
    /*** TODO: A COMPLETER ***/
}


static void afficher_unaire(struct noeud_ast *ast)
{
    UNUSED(ast);
    /*** TODO: A COMPLETER ***/
}


static void afficher_binaire(struct noeud_ast *ast)
{
    UNUSED(ast);
    /*** TODO: A COMPLETER ***/
}


/*---------------------------------------------------------------------------*/
/*     FONCTIONS D'EVALUATION                                                */
/*---------------------------------------------------------------------------*/


/* Definitions (locale) des fonctions "specifiques" 
 * Pas de verif sur le type d'ast, ces fonctions ont ete "branchees" sur
 * les noeud de type adequat lors de leur construction */

static struct patchwork *evaluer_valeur(struct noeud_ast *ast)
{
    struct patchwork* pt = creer_primitif(ast->data->u.val.nature);

    return pt;
}


static struct patchwork *evaluer_unaire(struct noeud_ast *ast)
{
    struct noeud_ast* oper = ast->data->u.oper.u.oper_un.operande;
    struct patchwork* eval_p = oper->evaluer(oper);
    struct patchwork* ret = creer_rotation(eval_p);
    liberer_patchwork(eval_p);
    return ret;
}


static struct patchwork *evaluer_binaire(struct noeud_ast *ast)
{
    struct noeud_ast* oper_g = ast->data->u.oper.u.oper_bin.operande_gauche;
    struct noeud_ast* oper_d = ast->data->u.oper.u.oper_bin.operande_droit;
    struct patchwork* eval_g = oper_g->evaluer(oper_g);
    struct patchwork* eval_d = oper_d->evaluer(oper_d);
    struct patchwork* ret = ast->data->u.oper.u.oper_bin.creer_patchwork(eval_g, eval_d);
    liberer_patchwork(eval_g);
    liberer_patchwork(eval_d);
    return ret;
}



/*---------------------------------------------------------------------------*/
/*     CREATION DES NOEUDS                                                   */
/*---------------------------------------------------------------------------*/

// C'est a la creation des noeuds que les "branchements" vers les fonctions
// adequats sont realises

struct noeud_ast *creer_valeur(const enum nature_primitif nat_prim)
{
    struct noeud_ast* node  = malloc(sizeof(struct noeud_ast));
    node->afficher = afficher_valeur;
    node->evaluer = evaluer_valeur;
    struct noeud_ast_data* data = malloc(sizeof(struct noeud_ast_data));
    data->nature = OPERATION;
    node->data = data;
    data->nom = NULL;
    data->nature = VALEUR;
    data->u.val.nature= nat_prim;
    data->u.val.creer_patchwork = creer_primitif;
    return node;
}


struct noeud_ast *creer_unaire(const enum nature_operation nat_oper,
			       struct noeud_ast *opde)
{
    struct noeud_ast* node  = malloc(sizeof(struct noeud_ast));
    node->afficher = afficher_unaire;
    node->evaluer = evaluer_unaire;
    struct noeud_ast_data* data = malloc(sizeof(struct noeud_ast_data));
    node->data = data;
    data->nature = OPERATION;
    data->nom = NULL;
    data->u.oper.arite = UNAIRE;
    data->u.oper.u.oper_un.operande = opde;
    if (nat_oper == ROTATION)
    {
	data->u.oper.u.oper_un.creer_patchwork = creer_rotation;
    }
    else
    {
	data->u.oper.u.oper_un.creer_patchwork = NULL;
    }
    return node;
}


struct noeud_ast *creer_binaire(const enum nature_operation nat_oper,
				struct noeud_ast *opde_g, 
				struct noeud_ast *opde_d)
{
    struct noeud_ast* node  = malloc(sizeof(struct noeud_ast));
    node->afficher = afficher_binaire;
    node->evaluer = evaluer_binaire;
    struct noeud_ast_data* data = malloc(sizeof(struct noeud_ast_data));
    node->data = data;
    data->nature = OPERATION;
    data->nom = NULL;
    data->u.oper.arite =BINAIRE;
    data->u.oper.u.oper_bin.operande_gauche = opde_g;
    data->u.oper.u.oper_bin.operande_droit = opde_d;
    if (nat_oper == JUXTAPOSITION)
    {
	data->u.oper.u.oper_bin.creer_patchwork = creer_juxtaposition;
    }
    else if (nat_oper == SUPERPOSITION)
    {
	data->u.oper.u.oper_bin.creer_patchwork = creer_superposition;
    }
    else
    {
	data->u.oper.u.oper_bin.creer_patchwork = NULL;
    }
    return node;
}


/*---------------------------------------------------------------------------*/
/*     LIBERATION DES NOEUDS                                                 */
/*---------------------------------------------------------------------------*/

void liberer_expression(struct noeud_ast *res)
{
    if (res == NULL)
    {
	return;
    }
    enum nature_noeud nat_node = res->data->nature;
    if (nat_node == OPERATION)
    {
	enum arite_operation ar_oper = res->data->u.oper.arite;
	if (ar_oper == UNAIRE)
	{
	    liberer_expression(res->data->u.oper.u.oper_un.operande);
	}
	else if (ar_oper == BINAIRE)
	{
	    liberer_expression(res->data->u.oper.u.oper_bin.operande_gauche);
	    liberer_expression(res->data->u.oper.u.oper_bin.operande_droit);
	}
    }
    free(res->data);
    free(res);
    UNUSED(noms_operations);
    UNUSED(noms_primitifs);
}
